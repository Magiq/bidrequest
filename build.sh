#!/usr/bin/env bash

COUNTRY_TAR_LINK="http://geolite.maxmind.com/download/geoip/database/GeoLite2-Country.tar.gz"
COUNTRY_TAR="country.tar.gz"
COUNTRY_TMP="country-tmp"

wget $COUNTRY_TAR_LINK -q --show-progress -O $COUNTRY_TAR
mkdir $COUNTRY_TMP
tar -zxf $COUNTRY_TAR -C $COUNTRY_TMP
cp $COUNTRY_TMP/$(ls $COUNTRY_TMP/|head -n 1)/*.mmdb country.mmdb
rm $COUNTRY_TAR
rm -rf $COUNTRY_TMP

wget https://raw.githubusercontent.com/ua-parser/uap-core/master/regexes.yaml 