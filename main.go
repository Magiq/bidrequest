package main

import (
	"bitbucket.org/Magiq/bidrequest/parser"
	"flag"
	"fmt"
	"log"
	"net/http"
)

func main() {
	port := flag.Int("port", 80, "webserver port to listen")
	userAgentFile := flag.String("useragents", "regexes.yaml", "user agents file")
	geoipCountryFile := flag.String("geoip", "country.mmdb", "geoip country file")

	flag.Parse()

	userAgentParser, err := parser.UserAgent(*userAgentFile)
	if err != nil {
		log.Fatalf("Can't init user agent parser: %s", err)
	}

	countryParser, err := parser.Country(*geoipCountryFile)
	if err != nil {
		log.Fatalf("Can't init country parser: %s", err)
	}

	http.Handle("/", &BidRequestHandler{
		userAgentParser: userAgentParser,
		countryParser:   countryParser,
	})
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", *port), nil))
}
