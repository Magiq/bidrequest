package parser

import (
	"github.com/ua-parser/uap-go/uaparser"
)

type UserAgentParser interface {
	Parse(string) (UserAgentResult, error)
}

type userAgentParser struct {
	parser *uaparser.Parser
}

type UserAgentResult struct {
	Os         string
	DeviceType string
	Browser    string
}

func (uap *userAgentParser) Parse(userAgent string) (UserAgentResult, error) {
	client := uap.parser.Parse(userAgent)

	return UserAgentResult{
		Os:         client.Os.Family,
		DeviceType: client.Device.Family,
		Browser:    client.UserAgent.Family,
	}, nil
}

func UserAgent(dbFile string) (*userAgentParser, error) {
	parser, err := uaparser.New(dbFile)
	if err != nil {
		return nil, err
	}

	return &userAgentParser{
		parser: parser,
	}, nil
}
