package parser

import (
	"github.com/oschwald/geoip2-golang"
	"net"
)

type CountryParser interface {
	ParseIP(string) (string, error)
}

type countryParser struct {
	db *geoip2.Reader
}

func (p *countryParser) ParseIP(ip string) (string, error) {
	parsedIP := net.ParseIP(ip)

	record, err := p.db.Country(parsedIP)
	if err != nil {
		return "", nil
	}

	return record.Country.Names["en"], nil
}

func Country(dbFile string) (*countryParser, error) {
	db, err := geoip2.Open(dbFile)

	if err != nil {
		return nil, err
	}

	return &countryParser{
		db: db,
	}, nil
}
