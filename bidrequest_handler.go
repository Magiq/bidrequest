package main

import (
	"bitbucket.org/Magiq/bidrequest/parser"
	"encoding/json"
	"github.com/bsm/openrtb"
	"log"
	"net/http"
	"net/url"
)

type BidRequestResponse struct {
	parser.UserAgentResult
	Country string
	Domain  string
}

type BidRequestHandler struct {
	userAgentParser parser.UserAgentParser
	countryParser   parser.CountryParser
}

func (h BidRequestHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var bidRequest *openrtb.BidRequest
	if err := json.NewDecoder(r.Body).Decode(&bidRequest); err != nil {
		log.Printf("Error handling json: %s", err)
		return
	}

	userAgent, err := h.userAgentParser.Parse(r.UserAgent())
	if err != nil {
		log.Printf("Can't parse user agent: %s", err)
	}

	country, err := h.countryParser.ParseIP(r.RemoteAddr)
	if err != nil {
		log.Printf("Can't parse country: %s", err)
	}

	u, err := url.Parse(bidRequest.Site.Page)
	if err != nil {
		log.Printf("Can't parse page: %s", err)
	}

	response := BidRequestResponse{
		UserAgentResult: userAgent,
		Country:         country,
		Domain:          u.Hostname(),
	}

	if err = json.NewEncoder(w).Encode(response); err != nil {
		log.Printf("Can't write response: %s", err)
	}
}
