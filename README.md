# Bidrequest

Simple app that parses BidRequest json and return parsed data in following format:

```
{"Os":"Linux","DeviceType":"Generic Smartphone","Browser":"Puffin","Country":"United Kingdom","Domain":"www.yoursite.com"}
```

## Installation

Clone repository or use `go get -u bitbucket.org/Magiq/bidrequest`

Run ./build.sh to download geoip and user agents database file

## Usage
`bidrequest -port=80` 

You can specify database files if they in another directory by geoip and useragents flags.

## Testing

For testing purpose there is basic bidrequest json file that can be used for curl, for example

`curl -H "User-Agent: Mozilla/5.0 (X11; U; Linux x86_64; en-us) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.114 Safari/537.36 Puffin/4.8.0.2965AP" -X POST -d @bid.json http://localhost`

Is is supposted that app runs in default 80 port